resource "google_compute_instance" "default" {
	count = "${length(var.name)}"
	name = "${element(var.name, count.index)}"
	machine_type = "${var.machine_type}"
	zone = "${var.zone}"
	tags = [ "${var.name}-firewall" ]
	boot_disk {
		initialize_params {
			image = "${var.image}"
		}
	}
	network_interface {
		network = "${var.network}"
		access_config {
			// Ephemeral IP
		}
	}
	metadata {
		sshKeys = "${var.connection_user}:${file("${var.public_key}")}"
	}
	provisioner "remote-exec" {
		connection = {
			type = "${var.connection_type}"
	            user = "${var.connection_user}"
	            private_key = "${file("var.private_key")}"
		}
		inline = [
				"${lookup(var.update_packages, var.package_manager)}",
				"${lookup(var.install_packages, var.package_manager)} ${join(" ", element(var.packages, count.index))}"
		]
	}

	provisioner "remote-exec" {
		connection = {
			type = "${var.connection_type}"
			user = "${var.connection_user}"
			private_key = "${file("${var.private_key}")}"
		}
		scripts = "${element(var.scripts, count.index)}" 
	}
}
