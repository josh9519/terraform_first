.PHONY = jenkins python_systemd-http-server vim_and_config
VAR_DIR = "variables"

jenkins:
	@bash scripts_and_packages "jenkins"
	@terraform apply -auto-approve -var-file=${VAR_DIR}/jenkins.tfvars
server:
	@bash scripts_and_packages "server"
	@terraform apply -auto-approve -var-file=${VAR_DIR}/server.tfvars

boxes = "jenkins" "server"
multi:
	@bash scripts_and_packages $(boxes)
	@terraform apply -auto-approve -var-file=${VAR_DIR}/jenkinsserver.tfvars

