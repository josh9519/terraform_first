name = "jenkinsserver"
packages = [ [ "wget", "java" ], [ "git" ] ]
scripts = [ "scripts/install_jenkins", "scripts/install_server" ]
