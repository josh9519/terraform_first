provider "google" {
	credentials = "${file("~/.gcp/not_my_terraform_key.json")}"
	project = "the-byway-214108"
	region = "europe-west2"
}
